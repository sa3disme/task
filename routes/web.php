<?php

use App\Http\Controllers\CodeController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/lowest-integer',[CodeController::class,'lowestIntegerPositiveValue']);
Route::get('/does-number-contain-five',[CodeController::class,'checkIfNumberContain5']);
Route::get('/order',[CodeController::class,'order']);