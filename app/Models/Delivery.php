<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Delivery extends Model
{
    use HasFactory;

    /**
     * Get Delivery order details.
     */
    public function orders()
    {
        return $this->morphMany('App\Order', 'orderable');
    }
}
