<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Dinein extends Model
{
    use HasFactory;

    /**
     * Get Takeaway order details.
     */
    public function orders()
    {
        return $this->morphMany('App\Order', 'orderable');
    }
    
}
