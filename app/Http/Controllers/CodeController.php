<?php

namespace App\Http\Controllers;

use App\Models\Delivery;
use App\Models\Dinein;
use App\Models\Order;
use App\Models\Takeaway;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CodeController extends Controller
{
    //
    public function lowestIntegerPositiveValue() {
        $array_values = [5,-9,-8,4,-9,4,7,10,7];
        //step 1 remove all negative number 
        $array_positive_values = array();
        foreach($array_values as $number){
            if($number > 0) {
                $array_positive_values[] = $number;
            }
        }
        sort($array_positive_values);
        $lowest_array_value = $array_positive_values[0];
        
        // step 2 find the missing lowest number 
        for($i =$lowest_array_value ; $i<= max($array_positive_values); $i++) {
            if(! in_array($i+1,$array_positive_values)){
                return $i+1;
            }
        }
        return $array_positive_values;
    }

    public function checkIfNumberContain5()
    {
        /*todo accept start & end numbers dynamically with validation */
        $start = 1;
        $end = 9;
        $count = 0;
        for($i = $start; $i <= $end; $i++) {
            if (! preg_match('~[5]+~', $i) ? true : false) $count++;
        }
       return $count;
    }

    public function order(Request $request)
    {
        $order = new Order();
        /*todo separate validation request */
        $validator = Validator::make($request->all(), [
            'type' => 'in:delivery,dine-in,takeaway',
        ]);
        $validator->sometimes(['table_number','waiter_name'], 'required|string', function ($input) {
            return $input->type == 'dine-in';
        });
        $validator->sometimes(['service_charge'], 'required|float', function ($input) {
            return $input->type == 'dine-in';
        });
        $validator->sometimes(['customer_phone_number','customer_name'], 'required|string', function ($input) {
            return $input->type == 'delivery';
        });

        if($type = 'dine-in') {
            $order = Dinein::create($request->all());
        }else if($type = 'delivery'){
            $order = Delivery::create($request->all());
        }else if($type = 'takeaway'){
            $order = Takeaway::create($request->all());
        }else {
            return "not valid";
        }
        $myOrder = new Order();
        $myOrder->orderable_id = $order->id;
        $myOrder->orderable_type = $request->type;
        $myOrder->save();

    }
}
